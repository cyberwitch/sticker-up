import requests
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.telegram.provider import TelegramProvider
from celery import shared_task
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist

from sticker_up.celery import SingletonTask
from stickers.models import StickerSet, Sticker
from stickers.utils import get_sticker_set, get_file_url, save_file, send_message
from telegram_bot.models import UpdateOffset


@shared_task(base=SingletonTask)
def poll():
    params = {'timeout': 150}
    update_offset = UpdateOffset.objects.first()
    if update_offset:
        params['offset'] = update_offset.offset

    try:
        response = requests.get(
            f'https://api.telegram.org/bot{settings.TELEGRAM_BOT_TOKEN}/getUpdates',
            params=params
        ).json()

        if len(response['result']):
            update_offset_id = 0
            for update in response['result']:
                handle_telegram_webhook.delay(update)
                update_offset_id = max(update_offset_id, update['update_id'])

            UpdateOffset.objects.update_or_create({'offset': update_offset_id + 1})
    except Exception as e:
        print(e)
    finally:
        poll.delay()


@shared_task
def handle_telegram_webhook(data):
    try:
        user = SocialAccount.objects.get(provider=TelegramProvider.id, uid=data["message"]["from"]["id"]).user
    except SocialAccount.DoesNotExist:
        send_message(
            data["message"]["chat"]["id"],
            f"Who dis? Import your stickers at https://{Site.objects.first().domain}!"
        )
    else:
        if "message" in data and "sticker" in data["message"]:
            sticker_set, _ = StickerSet.objects.get_or_create(name=data["message"]["sticker"]["set_name"])
            sticker_set.users.add(user)
            import_sticker_set.delay(sticker_set.pk, data["message"]["chat"]["id"])
        else:
            send_message(
                data["message"]["chat"]["id"],
                f"Hey! Just send me a sticker and I'll import the whole pack for you."
            )


@shared_task
def import_sticker_set(pk, chat_id):
    send_message(chat_id, "Awesome, I'll let you know when I'm done importing. This could take a few minutes...")

    sticker_set = StickerSet.objects.get(pk=pk)
    name = sticker_set.name
    data = get_sticker_set(name)["result"]

    if sticker_set.title != data["title"]:
        sticker_set.title = data["title"]
        sticker_set.save()

    if "thumb" in data:
        save_file(get_file_url(data["thumb"]["file_id"]), sticker_set.thumbnail)

    for sticker_data in data["stickers"]:
        try:
            sticker = sticker_set.stickers.get(emoji=sticker_data["emoji"])
        except ObjectDoesNotExist:
            sticker = Sticker.objects.create(sticker_set=sticker_set, emoji=sticker_data["emoji"])

        if "thumb" in sticker_data:
            save_file(get_file_url(sticker_data["thumb"]["file_id"]), sticker.thumbnail)

        save_file(get_file_url(sticker_data["file_id"]), sticker.sticker)

    send_message(chat_id, f"Got it! I finished importing \"{sticker_set.title}\"")
