from django.core.management.base import BaseCommand

from telegram_bot.tasks import poll


class Command(BaseCommand):
    def handle(self, *args, **options):
        poll.delay()
