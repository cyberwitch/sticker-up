from django.db import models


class UpdateOffset(models.Model):
    offset = models.IntegerField()
