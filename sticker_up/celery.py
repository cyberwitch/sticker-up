from __future__ import absolute_import, unicode_literals

import os

from celery import Celery, Task
from celery.signals import worker_ready
from celery.utils.log import get_task_logger
from django.core.cache import cache

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sticker_up.settings")

app = Celery("sticker_up")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


logger = get_task_logger(__name__)

LOCK_EXPIRE = 301  # One second more than the default celery task timeout


class SingletonTask(Task):
    def __call__(self, *args, **kwargs):
        lock = cache.lock(self.name, timeout=LOCK_EXPIRE)

        if not lock.acquire(blocking=False):
            logger.info("{} failed to lock".format(self.name))
            return

        try:
            super(SingletonTask, self).__call__(*args, **kwargs)
        except Exception as e:
            lock.release()
            raise e
        lock.release()


@worker_ready.connect
def at_start(sender, **kwargs):
    with sender.app.connection() as conn:
        sender.app.send_task('telegram_bot.tasks.poll', connection=conn)
