from django.urls import path

from authentication import views

urlpatterns = [
    path('api/me/', views.UserView.as_view()),
]
