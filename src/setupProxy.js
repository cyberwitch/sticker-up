const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(createProxyMiddleware('/api', {target: 'http://localhost:8000/'}));
  app.use(createProxyMiddleware('/accounts', {target: 'http://localhost:8000/'}));
  app.use(createProxyMiddleware('/admin', {target: 'http://localhost:8000/'}));
  app.use(createProxyMiddleware('/media', {target: 'http://localhost:8000/'}));
  app.use(createProxyMiddleware('/static/admin', {target: 'http://localhost:8000/'}));
  app.use(createProxyMiddleware('/static/rest_framework', {target: 'http://localhost:8000/'}));
};
