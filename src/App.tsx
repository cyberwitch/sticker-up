import axios from 'axios';
import React from 'react';
import TelegramLoginButton from 'react-telegram-login';

import './App.css';

function App() {
  const [user, setUser] = React.useState({first_name: undefined});

  const getUser = async () => {
    try {
      const {data} = await axios.get(`api/me/`);
      setUser(data);
    } catch {
      console.log('Need to log in.')
    }
  };

  React.useEffect(() => {
    getUser();
  }, []);

  const dataAuthUrl = `${window.location}accounts/telegram/login/`;

  let content = <TelegramLoginButton botName={process.env.REACT_APP_BOT_NAME} dataAuthUrl={dataAuthUrl}/>;

  if (user.first_name) {
    const message = `Hi, ${user.first_name}`;
    content = <div>{message}</div>;
  }

  return (
    <div className="App">
      <header className="App-header">
        {content}
      </header>
    </div>
  );
}

export default App;
